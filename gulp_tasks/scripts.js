const gulp = require('gulp');
const eslint = require('gulp-eslint');

const conf = require('../conf/gulp.conf');

gulp.task('scripts', scripts);

function scripts() {
  return gulp.src(conf.path.src('**/*.js'))
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(gulp.dest(conf.path.tmp()));
}

gulp.task('scripts:backend', scriptsBackend);

function scriptsBackend() {
  return gulp.src(conf.path.src('../backend/dist/backend.js'))
    .pipe(gulp.dest(conf.path.tmp()));
}

gulp.task('scripts:backend:dist', scriptsBackendDist);

function scriptsBackendDist() {
  return gulp.src(conf.path.src('../backend/dist/backend.js'))
    .pipe(gulp.dest(conf.path.dist()));
}

