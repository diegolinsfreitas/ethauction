(function () {

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider, menuServiceProvider) {

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('app', {
                url: '',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'vm',
                abstract: true
            }).state('app.home', {
                url: '/',
                templateUrl: 'app/main/home.html',
                data: {
                    title: 'Store'
                }
            });

        menuServiceProvider.addItem({
            name: 'Home',
            icon: 'dashboard',
            sref: '.home'
        })
    };

    angular.module('app').config(config);
})();