(function () {
    'use strict';

    function menuService() {
        var menuItems = [

        ];

        return {
            addItem: function (item) {
                menuItems.push(item);
            },
            $get: function () {
                return {
                    loadAllItems: function () {
                        return menuItems;
                    }
                };
            }

        };
    }

    angular.module('app')
        .provider('menuService', menuService);

})();