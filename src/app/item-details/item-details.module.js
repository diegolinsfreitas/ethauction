(function () {

    angular.module('app').config(config);

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app.item-details', {
                url: '/details/:itemId',
                templateUrl: 'app/item-details/item-details.html',
                data: {
                    title: 'Details'
                }
            });

    };
    
})();
