(function () {



    /** @ngInject */
    function config($stateProvider, $urlRouterProvider, menuServiceProvider) {

        $stateProvider
            .state('app.item-listing', {
                url: '/market',
                templateUrl: 'app/item-listing/item-listing.html',
                data: {
                    title: 'Market'
                }
            });

        menuServiceProvider.addItem({
            name: 'Market',
            icon: 'dashboard',
            sref: 'app.item-listing'
        })
    };
    angular.module('app').config(config);
})();
