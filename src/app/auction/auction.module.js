(function () {

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider, menuServiceProvider) {

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('app.create-auction', {
                url: '/create-auction',
                templateUrl: 'app/auction/auction-form.html',
                data: {
                    title: 'New Auction'
                }
            });

        menuServiceProvider.addItem({
            name: 'Create Auction',
            icon: 'dashboard',
            sref: 'app.create-auction'
        })
    };

    angular.module('app').config(config);
})();